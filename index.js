var http = require('http'),
    queue = require('./queue.js');

http.createServer(function (req, res) {

  var respond = function (code, body) {
    res.writeHead(code.toString(), {'Content-Type': 'application/json'});
    res.end(JSON.stringify(body));
  },
  processBody = function (callback) {

    var body = '';

    req.on('data', function (data) {
      body = body + data;
      if (body.length > Math.pow(5, 10)) {
        req.connection.destroy();
        return respond(400, 'POST or PUT body was too long: ' + body.length);
      }
    });
    req.on('end', function () {
      var err;

      try {
        return callback(JSON.parse(body));
      } catch (e) {
        err = e;
      }

      return respond(400, err);

    });

  },
  processURL = function () {
    var params = req.url.split('/');
    params.shift();
    return params[0];
  },
  id = processURL();

  if (String(req.method).toUpperCase() === 'POST') {

    processBody(function (data) {
      queue.push(data, function (err, message) {
        if (err) {
          return respond(400, err);
        }
        return respond(201, message);
      });
    });

  } else if (String(req.method).toUpperCase() === 'GET') {

    queue.status(id, function (err, message) {
      if (err) {
        return respond(400, err);
      }
      return respond(200, message);
    });

  } else if (String(req.method).toUpperCase() === 'PUT' && id) {

    processBody(function (data) {
      if (!data.id) {
        data.id = id;
      }
      queue.update(data, function (err, message) {
        if (err) {
          return respond(400, err);
        }
        return respond(200, message);
      });
    });

  } else if (String(req.method).toUpperCase() === 'DELETE' && id) {

    queue.destroy(id, function (err, message) {
      if (err) {
        return respond(400, err);
      }
      return respond(200, message);
    });

  }

}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
