var q = [], // q for maintaining order--an array of uuids
    done = [], // queue for done tasks
    obj = {}, // obj for actual data.
    doneObj = {}, // object for the done tasks
    uuid = require('uuid'),  // UUID is so we can have reference to a queue item regardless of queue position.
    events = require('events'),
    eventEmitter = new events.EventEmitter(),
    isNumber = function (str) { // convenience for checking uuid vs array index
      return str === Number(str);
    },
    remove = function (id, status, callback) {
      var task, data, index, err = null;
      if (isNumber(id)) { // then it's an index.
        index = id;
      } else {
        index = q.indexOf(id);
      }

      if (!q[index]) {
        err = {error: 'Queue needs a valid event id or index to search on.'};
      } else if (!status) {
        err = {error: 'No updated status.'};
      }

      if (!err) {
        task = q.splice(index, 1);
        data = obj[task];
        data.status = status;
        delete obj[task];
        if (status === 'Completed') {
            data.position = done.length;
            done.push(data.id);
            doneObj[data.id] = data;
        }

      }
      return callback(err, data);
    };

module.exports = {

  push: function (data, callback) {
    var err = null,
        length = q.length,
        id = uuid.v4();

    data.id = id;
    data.status = 'Added';
    data.position = length; // position here is just to respond to the user.
    try {
      q.push(data.id);
    } catch (e) {
      err = e;
    }
    obj[id] = data;

    return callback(err, data);

  },

  update: function (data, callback) {
    var i, err = null;
    if (data && data.id) {
      for (i in data) {
        if (data.hasOwnProperty(i)) {
          obj[data.id][i] = data[i];
        }
      }
      if (data.status && data.status === 'Completed') {
        return remove(data.id, 'Completed', callback);
      }
    } else {
      err = {error: 'Missing data for update'};
    }

    return callback(err, obj[data.id]);
  },

  status: function (id, callback) {
    var err = null, val;

    if (id && obj[id]) {
      obj[id].position = q.indexOf(id);
      val = obj[id];
    } else if (id && doneObj[id]) {
      doneObj[id].position = done.indexOf(id);
      val = doneObj[id];
    } else if (id === 'completed') {
      val = doneObj;
    } else if (!id && obj) {
      val = obj; // get all objects in queue
    } else {
      err = {error: 'That task doesn\'t exist.'};
    }

    return callback(err, val);

  },

  destroy: function (id, callback) {

    return remove(id, 'Deleted', callback);

  }

};
