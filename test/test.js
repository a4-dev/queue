var server = require('../index'), // required to lift the server
    expect = require('chai').expect,
    request = require('superagent'),
    url = 'http://127.0.0.1:1337/';

describe('Queue Tests', function () {

  /**
   * test suite for the 'POST' functionality.  This is only one way to organize
   * tests, you're welcome to organize any way you see fit.
   */
  describe('POST', function () {

    /**
     * One test covering the 'POST' functionality.
     * - it is not necessarily a good test :)
     */
    it('should post an event', function (done) {
      var firstEvent = {name: 'my event', value: 'test value'};

      request
        .post(url)
        .send(firstEvent)
        .end(function (err, res) {
          expect(err).to.not.exist;
          expect(res.body).to.be.ok;
          done();
        });

    });

  });

});
