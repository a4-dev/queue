# README #

This is a simple message/event queue for demonstration and testing.

### How do I get set up? ###

* Requirements: [nvm](https://github.com/creationix/nvm) (node version manager, for easy install/switching of different versions), [node.js](http://nodejs.org), [npm](https://github.com/npm/npm) (this is included with Node.js), [git](https://git-scm.com), and a text editor and terminal program you like.
* Optional: [Postman](https://www.getpostman.com) (for interacting with the queue API, without needing a browser-based front-end app)

* To get this running on your development machine:
    * clone the repository here, or download from the link at the top of this page.
    * install [nvm](https://github.com/creationix/nvm) (installation instructions in the link)
    * install the version of node that we're using: ```nvm install 5```
    * from your terminal program, change context to the repo's directory with ```cd queue```
    * Install node dependencies: ```npm install```

### Using the message queue ###

The message queue is a combination of API (in index.js) and queuing engine (in the queue.js file).  The API receives the typical CRUD requests, and creates, reads from, updates, or deletes a message/event in its internal queue.

* To create, POST a JSON object (with any internal structure or content) to http://127.0.0.1:1337.  The API will save this to the internal queue, and return the same object with some new fields: id (a uuid4), status ("Added"), and position (its order in the queue).
* To read the status of an object in the queue, GET http://127.0.0.1:1337/{uuid}. (If the object isn't in the to-be-done queue, but is in the completed-tasks, queue, the API will return it from the completed queue.
* To read the status of all objects in the queue, GET http://127.0.0.1:1337/.
* To read the status of all objects in the completed queue, GET http://127.0.0.1:1337/completed.
* To update an object in the queue, PUT to http://127.0.0.1:1337/{uuid}. The body of the put can be partial: any properties you don't put (but which are in the queued object) won't be updated.  The updated object will be returned as the response body.
* To mark an object in the queue as "Completed," PUT to http://127.0.0.1:1337/{uuid} with the request body {"status": "Completed"}.
* To delete an object from the queue, DELETE http://127.0.0.1:1337/{uuid}. The object will be removed from the queue, and returned as the response body.

You can see the POST/GET/PUT/DELETE methods in the index.js file, and you can see that they each call different methods of the queue module.  (POST and PUT also iterate over the request body to make sure the request is complete before sending the body to the queue.)

Note that this is a simulated queue: it's not persistent.  Anytime you restart the server, the queue is lost.  (That could be fixable by a dependency on a database of some kind; that's out of scope for this demonstration.)

* Start the server (for use with something like Postman) with ```node index.js```
  * You could run this more conveniently with nodemon, if you like.
    * First ```npm install -g nodemon```
    * Then ```nodemon index.js```

### Testing ###

Run tests with ```npm test```.  Your goal is to write tests covering as much of the API's functionality as you can.
